var express = require('express');
var app = express();

var port = process.env.PORT || 8085;
var logger = require('morgan');
var methodOverride = require('method-override');
var bodyParser = require('body-parser');
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb',extended: true}));
var path = require('path');
var formidable = require('formidable');
var phantom = require('phantom');
var fs = require('fs');

var allowCrossDomain = function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
  if ('OPTIONS' == req.method) {
    res.send(200);
  }
  else {
    next();
  }
};


app.use(logger('dev'));
app.use(bodyParser.json());
app.use(methodOverride());
app.use(allowCrossDomain);
app.use('/static', express.static(__dirname + '/static'));


app.post('/api/declaracion', function(req, res) {

  var url = "./static/pdf/" + 'informe1.html';
  var html= req.body.html;
  var docName = req.body.archivo;
 //crea el archivo HTML a exportar
  var streamEscritura = fs.createWriteStream('./static/pdf/informe1.html', 'utf-8');
  streamEscritura.write(html);
  streamEscritura.end();
  //realiza la exportacion a PDF
  phantom.create(['--ignore-ssl-errors=yes']).then(ph => {
    _ph = ph;
    return _ph.createPage();
  }).then(page => {
    _page = page;
    return _page.property('onConsoleMessage', function (msg) {
    });
  }).then(() => {
    return _page.property(
      'paperSize',
      {
        format: 'A4',
        orientation: 'portrait',
        margin: {top: '50px', left: '50px', right: '50px', bottom: '50px'}
})
  }).then(() => {
    return _page.property('viewportSize', {width: 1920, height: 1080});
  }).then(() => {
    return _page.property('dpi', 72)
  }).then(() => {
    return _page.open(url);
  }).then(status => {
    return _page.render('./static/pdf/genereted_reports/'+docName);
  }).then(() => {
    _page.close();
    _ph.exit();
    res.sendStatus(200);
  });
});

app.get('/api/declaracion', function (req, res) {
  var pdf = 'prueba.pdf';
  var filePath = "/static/pdf/genereted_reports/" + pdf;
  fs.readFile(__dirname + filePath , function (err,data){
    res.send(data.toString('base64'));
  });
});

app.listen(port);
console.log("APP por el puerto " + port);
